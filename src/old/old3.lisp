(in-package :hc)

#|
This is a new iteration of my hypercube game. One major change has prompted this
version. I am now using closures to produce functions for the structure of the
parts. This should result in a much faster simulation where the only thing being
done during the simulation is bit-nand calculations. Previous versions were a
few orders of magnitude slower. At some point I will make a couple tests to
demonstrate exactly how slow they were, but that comes later.

There are currently only a couple functions. Parts are defined in defpart,
which adds them to the *part* global variable. Parts have specific parameters
depending on the part. Inputs and consts only have a state part, tho that needs
to be changed for input. Nand gets its state through compilation of the lookup
of the parts it is connected to, then bit-nanding those values. That compilation
function does the heavy lifting. Lookup is done at the end by funcalling the
compiled function.

Lots of work left to do.
  Make a pool for storing parts
  Add a part type for previously made parts
  Test some more complex parts like an XOR
|#


(defvar *part*)

(defun defpart (&rest args)
  "Create a new part. First arg is the name, second arg is the type, rest of the
  args depend on which type."
  (dbg 'hc "Entered defpart:")
  #| args is (name type &rest etc)
     etc depends on type
     input: in
     nand:  in1 in2
     const: state
     part
  |#
  ;; Error handling
  (case (second args)
    (const
     (unless (simple-bit-vector-p (third args))
       (error "Const type requires a bit-vector state."))
     (when (> (length args) 3)
       (error "Const should only have three parameters.")))
    (nand
     (unless (and (symbolp (third args))
                  (symbolp (fourth args)))
       (error "Nand type requires symbols in both in1 and in2."))
     (when (> (length args) 4)
       (error "Nand should only have four parameters.")))
    (input
     (when (> (length args) 3)
       (error "Input should only have two parameters.")))
    (part
     (when (> (length args) 2)
       (error "Part should only have two parameters."))))
  (dbg 'hc " Name: ~A~% Type: ~A~% Other args: ~A"
       (first args) (second args) (rest (rest args)))
  (push args *part*)
  args)

(defmacro defparts (part-list)
  "Define multiple parts at a time."
  (let ((parts (loop for part in part-list
                     collecting `(defpart ,@part))))
    `(progn ,@parts)))

(defun clear-part ()
  "Reset the part. Due to using lexical scope, this shouldn't ever be necessary
  for the user to call."
  (dbg 'hc "Entered clear-part:~% Contained: ~A" *part*)
  (setf *part* nil))

(defun compile-part (name)
  "Compile the part into a function that will walk down the part-tree to find
  the constants and/or inputs, to simulate that part."
  (dbg 'hc "Entered compile-part:~% Outer part: ~A" name)
  (labels ((sub-compile (name)
             (dbg 'hc " Compiling: ~A" name)
             (with-part name
               (print part)
               (let ((type (second part)))
                 (dbg 'hc " Type: ~A" type)
                 (let ((return-fn
                         (case type
                           (const
                            (let ((state (third part)))
                              #'(lambda () state)))
                           (input
                            (get-input part))
                           (nand
                            (let ((in1 (sub-compile (third part)))
                                  (in2 (sub-compile (fourth part))))
                              #'(lambda ()
                                  (bit-nand (funcall in1) (funcall in2)))))
                           (t (error "Part type ~A not defined." type)))))
                   (dbg 'hc " Leaving: ~A" name)
                   return-fn)))))
    (list (sub-compile name) *part*)))

(defun run-part (compiled-part &optional lex-env)
  (dbg 'hc "Entered run-part:")
  (let ((*part* (if lex-env lex-env (second compiled-part))))
    (funcall (first compiled-part))))

(defmacro with-compiled-part (compiled-part &body body)
  `(let ((*part* (second ,compiled-part)))
     (progn ,@body)))

(defmacro with-part (name &body body)
  "Find the part in the *parts* list and return it the run body, error if part
  is non-existent."
  `(let ((part (assoc ,name *part*)))
     (if (null part)
         (error "~A part not found." ,name)
         (progn ,@body))))
  ;; `(let ((part (assoc ,name *part*)))
  ;;    (if (null part)
  ;;        (error "~A~% part not found" ,name)
  ;;        (progn ,@body))))

(defmacro with-input (input compiled-part bit &body body)
  "Set the input within the part to the given bit and run body."
  `(with-compiled-part ,compiled-part
     (with-part ,input
       (rplacd part (list (second part) ,bit))
       (progn ,@body))))

(defun part-inputs (part) ;; TODO use with-compiled-part instead
  "Return the list of names of inputs that the part connects down to."
  (dbg 'hc "Entered part-inputs:")
  (let (acc)
    (labels ((rec (part)
               (with-part part
                 (let ((type (second part)))
                   (case type
                     (input (push (first part) acc))
                     (const nil)
                     (nand (rec (third part))
                           (rec (fourth part)))
                     (part (loop :for sub-part :in part
                                 :do (rec part))))))))
      (rec part))
    (if acc
        (dbg 'hc " Found: ~A" acc)
        (dbg 'hc " No inputs found."))
    acc))

(defun test-part ()
  "Test all possible inputs for the part and return a truth table of the input
  to output relationships."
  ())

(defvar *pool* (make-hash-table))

(defun clear-pool ()
  "Reset the pool to an empty hash table."
  (setf *pool* (make-hash-table)))

(defun take-part (new-name output-part)
  "Take the part from *parts* and move it to the *pool* using the new-name.
  Output part is the part whose state will be used as the output."
  (setf (gethash new-name *pool*)
        (append (list 'part) (part-inputs output-part))))
 

(defun test-not ()
  (let ((*part* nil))
    (defparts
        (('in 'input)
         ('one 'const #*1)
         ('nand 'nand 'in 'one)))
    (compile-part 'nand)))
    ;; (unless
    ;;   (or (assert (equal (funcall (compile-part 'nand)) #*0))
    ;;       (assert (equal (funcall (compile-part 'nand)) #*1)))
    ;;   'passed)))


