(in-package #:hc)

;;;; Michael Reis
;;;; <michael@mtreis86.com>

#| This is the start of a game I have been thinking about for a while. One
aspect of it anyways. The basic idea is to have a couple layers to play puzzles
in where the puzzles are basically building a computer at various levels.
One level would be nands, the next would maybe be lambda calculus, the last
would be a lisp built on top of the other two. Or maybe just nands and lisp.
This is the simulator for that system.
|#

(defclass unit ()
  ((state :initform (make-array 1 :element-type 'bit)
          :initarg :state :accessor state))
  (:documentation
   "A unit has a bit array state of length 1."))

(defclass bus ()
  ((state :initform (make-array 2 :element-type 'bit)
          :initarg :state :accessor state))
  (:documentation
   "A bus has a bit array state greater than 1."))

(defclass nand (unit)
  ((in1 :initform nil :accessor in1)
   (in2 :initform nil :accessor in2))
  (:documentation
   "A nand is a unit with two inputs whose state is the NOT of ANDing them."))

(defclass input () ()
  (:documentation
   "An input is a type of unit within a part that outputs to parts within."))

(defclass output () ()
  (:documentation
   "An output is a type of unit within a part that presents the state of some
   connected inner parts as an output available from the part."))

(defclass input-unit (unit input) ())

(defclass input-bus (bus input) ())

(defclass output-unit (unit output) ())

(defclass output-bus (bus output) ())

(defclass const () ()
  (:documentation
   "A const is an input with constant state."))

(defclass const-unit (const unit) ())

(defclass const-bus (const bus) ())

(defclass part ()
  ((inputs :initform (make-hash-table) :reader inputs)
   (parts :initform (make-hash-table) :reader parts)
   (outputs :initform (make-hash-table) :reader outputs)
   (name :initform nil :initarg :name :accessor name))
  (:documentation
   "A part is a group of parts and/or nands with connections between them."))

(defclass pool () ()
  (:documentation
   "A pool is a group of parts or units without connections."))

(defclass nand-pool (pool)
  ((free-nands :initform nil :accessor free-nands))
  (:documentation
   "A nand-pool is the group of nands that are available to be used
   in a process. Saved as a list since nands are all equivalent."))

(defclass part-pool (pool)
  ((free-parts :initform nil :accessor free-parts))
  (:documentation
   "A part-pool is a group of parts that are available to be used
   in a process."))

(defun make-nand ()
  "Make a new nand."
  (make-instance 'nand))

(defun make-nand-pool (count)
  "Initialize a nand pool with the given number of nands."
  (let ((pool (make-instance 'nand-pool)))
    (setf (free-nands pool) (loop for index from 0 below count
                                  collecting (make-nand)))
    pool))

(defun make-const-unit (bit)
  "Make a const with the bit as the state."
  (if (or (= bit 0)
          (= bit 1))
      (make-instance 'const-unit :state (int->bit-array bit))
      (error "Const unit must be a bit, 0 or 1.")))

(defun make-const-bus (integer)
  "Make a const with the integer as the state."
  (if (minusp integer)
      (error "Integer must be positive.")
      (make-instance 'const-bus :state (int->bit-array integer))))

(defun make-part (name)
  "Initialize an empty part."
  (make-instance 'part :name name))

(defun make-part-pool ()
  "Make a pool to store parts in."
  (make-instance 'part-pool))

(defun make-input-busses (part count)
  (loop for index from 0 below count do
    (setf (gethash (make-instance 'input-bus) (inputs part)) 'free)))

(defun make-input-units (part count)
  (loop for index from 0 below count do
        (setf (gethash (make-instance 'input-unit) (inputs part)) 'free)))



(defgeneric connect (from to)
  (:documentation
   "Connect the input of to to the output of from."))

(defmethod connect :before ((from nand) (to nand))
        (when (and (in1 to) (in2 to))
         (error "Nand must have an empty input slot before connecting.")))

(defmethod connect ((from nand) (to nand))
  "Connect the inputs of the nand2 to output of nand1 within the part."
  (if (null (in1 to))
      (setf (in1 to) from)
      (setf (in2 to) from)))

(defmethod connect :before ((in input-unit) (nand nand))
  (when (and (in1 nand) (in2 nand))
    (error "Nand must have an empty input slot before connecting.")))

(defmethod connect ((in input-unit) (nand nand))
  "Connect one of the inputs to the nand, unless both inputs are used."
  (if (null (in1 nand))
      (setf (in1 nand) in)
      (setf (in2 nand) in)))


(defgeneric update (type)
  (:documentation "Update the state based on inputs."))

(defmethod update ((nand nand))
  "Set the state of the nand based on its inputs' states."
  (setf (state nand) (bit-nand (state (in1 nand))
                               (state (in2 nand)))))

;; (defmethod update ((part part))
;;   "Set the state of the part based on its inputs' states and the connections
;;   of the nands within it."
;;   ())


(defgeneric grab (type location)
  (:documentation
   "Return a part of the given type from the location. Type could be a
   part, unit, or const. Location could be a part, container, or pool."))

(defmethod grab :before ((nand (eql 'nand)) (pool pool))
  (when (null (free-nands pool))
    (error "No free nands left in pool.")))

(defmethod grab ((nand (eql 'nand)) (pool pool))
  "Grab a nand from the pool."
  (let ((nand (first (last (free-nands pool)))))
    (setf (free-nands pool) (butlast (free-nands pool)))
    nand))
 
(defmethod grab ((part (eql 'part)) (pool pool))
  (cond ((null (free-parts pool))
         (error "No parts left in the pool."))
        ((null (gethash part (free-parts pool)))
         (error "No parts of that type remain in the pool."))))

(defmethod grab ((part (eql 'part)) (pool pool))
  "Grab a part from the pool."
  (let ((part (gethash part (free-parts pool))))
    (remhash part (free-parts pool))
    part))

(defmethod rem-part (part pool)
  "Remove the part from the pool."
  (remhash part (free-parts pool))
  pool)

(defmethod grab :before ((input (eql 'input-unit)) (part part))
  (when (null (inputs part))
    (error "Part doesn't have any inputs.")))

(defmethod grab ((input (eql 'input-unit)) (part part))
  "Grab a free input of the part"
  (maphash #'(lambda (key value)
               (when (eql value 'free)
                 (return-from grab key)))
           (inputs part)))


;;; Utilities

(defun int->bit-array (integer)
  "Convert int to bit-array. If int is 1 or 0, use a one-length array, if below
  256 use a byte, otherwise use 64bit. Integer must be positive."
  (if (minusp integer)
      (error "Integer must be positive.")
      (let* ((bit-length (cond ((or (= integer 0) (= integer 1)) 1)
                               ((< integer 256) 8)
                               (t 64)))
             (array (make-array bit-length :element-type 'bit)))
        (loop for bit-index from (- bit-length 1) downto 0
              for array-index from 0 below bit-length do
          (setf (aref array array-index)
                (ldb (byte 1 bit-index) integer)))
        array)))


;;; Integration Tests

(deftest test-nand ()
  (let ((nand (make-nand))
        (zero (make-const-unit 0))
        (one (make-const-unit 1)))
    ;; 0 0 nand = 1
    (setf (in1 nand) zero
          (in2 nand) zero)
    (update nand)
    (check (equal #*1 (state nand)))
    ;; 0 1 nand = 1
    (setf (in1 nand) zero
          (in2 nand) one)
    (update nand)
    (check (equal #*1 (state nand)))
    ;; 1 0 nand = 1
    (setf (in1 nand) one
          (in2 nand) zero)
    (update nand)
    (check (equal #*1 (state nand)))
    ;; 1 1 nand = 0
    (setf (in1 nand) one
          (in2 nand) one)
    (update nand)
    (check (equal #*0 (state nand)))))

(deftest test-not ()
  (let ((t-not (make-part 'not))
        (zero (make-const-unit 0))
        (one (make-const-unit 1))
        (t-nand (make-nand)))
    ()))




