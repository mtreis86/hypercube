(in-package :hc)

#|
A couple restrictions apply:

In effect this is an emulator of a hypothetical processor with an unusual
architecture. The sole logical component of the cpu is nands connected through
quantum entaglement in a manner where they can be arbitrarily re-arranged.

This is intended to be made into a game in the style of zactronics' games. There
will be two effective levels, one is nand-based low-level logic, the other is
high-level using a Lisp. In the game both will have a collection of puzzles to
solve with some sort of comparison between players. The low and high levels will
be available simultaneously.

Since the goal is to have a large number of nands being simulated, the main
work I have focused on has been making the compile-gate function, as having
the functions already compiled before running should greatly speed up the
resulting emulation.
|#

(defclass part ()
  ((struct) (funct) (state) (inputs)))

(defmacro defpart (name (args) (&key (struct nil) (funct nil) (state nil) (inputs nil)))
  (with-gensyms (structure name-as-keyword)
    (let ((name-as-keyword (as-keyword name)))

      `(progn (defclass ,name (part)
                structure)
              (defmethod make-part ,name (part-type (eql ,name-as-keyword))
                (make-instance part-type
                               :name ,name))
              (defmethod compile-part (,name))))))

(defmacro slot-builder (name initform initarg rw)
  (let ((name-as-unexported (as-unexported-symbol name))
        (name-as-keyword (as-keyword name))
        (to-rw (ecase rw
                 (r ':reader)
                 (w ':writer)
                 (rw ':accessor))))
    `(,name-as-unexported :initform ,initform :initarg ,initarg
            ,to-rw ,name-as-keyword)))

(defgeneric make-part (name type args)
  (:documentation ""))

(defgeneric compile-part (part)
  (:documentation ""))

;; (defpart const (int)
;;   (:funct (lambda (int) (int->bit-vec int))))

;; (defpart nand (in1 in2)
;;   (:funct (lambda (in1 in2) (bit-nand in1 in2))))








(defun as-keyword (symb)
  (intern (format nil "~A" symb) :keyword))

(defun as-unexported-symbol (symb)
  (intern (format nil "%~A" symb)))

(defun int->bit-vec (integer &key size)
  "Convert int to a simple bit vector. If size is specified will return a vector
  of that length, otherwise will return the shorted valid vector for that int."
  (let ((bit-length (or size (max 1 (ceiling (sqrt integer))))))     
    (cond ((minusp integer)
           (error "Integer must be positive."))
          ((and size (> bit-length size))
           (error "Specified size is not large enough to hold the resulting
                   vector"))
          (t (let ((array (make-array bit-length
                                     :adjustable nil
                                     :fill-pointer nil
                                     :element-type 'bit)))
               (loop for bit-index from (- bit-length 1) downto 0
                     for array-index from 0 below bit-length do
                       (setf (aref array array-index)
                             (ldb (byte 1 bit-index) integer)))
               (dbg 'hc "Converted ~A to ~A~%" integer array)
               array)))))






































