;;;; Hypercube
;;;; by Michael Reis
;;;; <michael@mtreis86.com>

(defsystem #:hypercube
  :version "0.0.0"
  :author "Michael Reis"
  :license "AGPL3.0"
  :depends-on ()
  :components ((:module "src"
                :components
                ((:file "packages")
                 (:file "debugging") ; From PAIP - See license LICENSE-PAIP
                 (:file "testing") ; From PCL - See license LICENSE-PCL
                 (:file "hypercube" :depends-on ("debugging"))
                 ;; (:file "parts" :depends-on ("debugging"))
                 ;; (:file "pool" :depends-on ("debugging" "parts"))
                 (:file "tests" :depends-on ("testing" "hypercube")))))
  :description "A nand-to-lambda-to-lisp puzzle game.")
